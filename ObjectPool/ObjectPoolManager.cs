﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ObjectPoolManager : MonoBehaviourSingleton<ObjectPoolManager>
{
    /// <summary>
    /// 物件池字典
    /// string:被回收的物件類別(key)
    /// object:被回收的物件實體(val)
    /// </summary>
    private Dictionary<string, object> poolList = new Dictionary<string, object>();

    private Dictionary<string, Transform> poolDrawer = new Dictionary<string, Transform>();

    public void CreatCustomPool(GameObject prefab, string typeName)
    {
    }

    public void Recycle<T>(T type, string typeName = "")
    {
        Queue<T> pool = GetPool(type, typeName);

        pool.Enqueue(type);
        string name = typeName.Length > 0 ? typeName : type.GetType().Name;
        (type as MonoBehaviour).transform.SetParent(poolDrawer[name]);
        (type as MonoBehaviour).gameObject.SetActive(false);
    }

    public T Reuse<T>(T type, string typeName = "")
    {
        Queue<T> pool = GetPool(type, typeName);

        if (pool.Count > 0)
        {
            //Debug.Log("物件池抽取：" + typeName + " / 庫存：" + pool.Count);
            return pool.Dequeue();
        }
        else
        {
            return default;
        }
    }

    private Queue<T> GetPool<T>(T type, string typeName = "")
    {
        string name = typeName.Length > 0 ? typeName : type.GetType().Name;

        if (!poolList.ContainsKey(name))
        {
            poolList.Add(name, new Queue<T>());

            Transform drawer = new GameObject(name).transform;
            drawer.SetParent(transform);
            poolDrawer.Add(name, drawer);
        }

        return (Queue<T>)poolList[name];
    }
}