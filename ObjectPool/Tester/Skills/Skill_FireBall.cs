﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill_FireBall : SkillBase
{
    public override void TriggerChecker()
    {
        //寫入檢測邏輯
        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0)
        {
            TriggerAction();
        }
    }

    public override void ReActive(Transform transform, Vector3 offset)
    {
        base.ReActive(transform, offset);
        lifeTime = 3;
    }
}
