﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// 技能基底(抽象類別)
/// </summary>
public abstract class SkillBase : MonoBehaviour
{
    /// <summary>
    /// 附加功能集合變數
    /// </summary>
    private UnityAction actions;

    /// <summary>
    /// 技能飛射速度
    /// </summary>
    [Header("技能飛射速度")]
    public float moveSpeed = 1f;

    public bool canFly { get { return moveSpeed != 0; } }

    /// <summary>
    /// 技能存活時間
    /// </summary>
    [Header("施放後技能存活時間")]
    public float lifeTime = 3f;

    /// <summary>
    /// 技能是否立即觸發
    /// </summary>
    [Header("施放後立即觸發")]
    public bool isImmediately = false;

    /// <summary>
    /// 回收延遲時間(預設1S)
    /// </summary>
    [Header("觸發後回收延遲時間")]
    public float recoveryDelay = 1f;

    private bool triggered;

    /// <summary>
    /// 是否已觸發效果
    /// </summary>
    public bool isTriggered
    {
        get
        {
            return triggered;
        }
        private set
        {
            triggered = value;
            if (!triggered)
            {
                transform.SetParent(null);
                gameObject.SetActive(true);
            }
        }
    }

    // Start is called before the first frame update
    private void Start()
    {//加入各種條件的功能
        if (canFly) actions += FlySkill;
    }

    // Update is called once per frame
    private void Update()
    {//執行所有在actions中的功能
        actions();
        //撰寫虛擬方法，技能運行中邏輯
        InActive();
    }

    /// <summary>
    /// 技能運行邏輯
    /// </summary>
    public void InActive()
    {
        if (isImmediately)
        {//立即觸發
            TriggerAction();
        }
        else
        {//運行檢測觸發邏輯
            TriggerChecker();
        }
    }

    /// <summary>
    /// 觸發檢測邏輯
    /// (抽象方法：不預先撰寫內容，繼承後必須定義內容)
    /// </summary>
    public abstract void TriggerChecker();

    /// <summary>
    /// 觸發後執行的事件
    /// (虛擬方法：預先寫入內容，可繼承使用或複寫)
    /// </summary>
    public virtual void TriggerAction()
    {
        if (isTriggered) return;
        isTriggered = true;
        Debug.Log("Skill Active .");
        Invoke("Recovery", recoveryDelay);
    }

    /// <summary>
    /// 回收物件功能
    /// </summary>
    public void Recovery()
    {
        //Destroy(gameObject);
        //使用物件池回收(物件池成立前暫時使用直接摧毀)
        ObjectPoolManager.Instance.Recycle(this);
    }

    /// <summary>
    /// 重新激活技能
    /// </summary>
    /// <param name="transform">激活對象的位置</param>
    /// <param name="offset">激活後偏移調整</param>
    public virtual void ReActive(Transform transform, Vector3 offset)
    {
        isTriggered = false;
        this.transform.position = transform.position + offset;
        this.transform.rotation = transform.rotation;
    }

    #region 功能設計

    /// <summary>
    /// 技能飛行功能(虛擬)
    /// </summary>
    public virtual void FlySkill()
    {//向自己的前方移動
        transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
    }

    #endregion 功能設計
}