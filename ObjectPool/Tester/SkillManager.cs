﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillManager : MonoBehaviour
{
    /// <summary>
    /// 技能預製物件欄位
    /// </summary>
    public List<SkillBase> skills;

    // Start is called before the first frame update
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
        //按鍵1：使用清單的第一個技能
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            ActivateSkill(0);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            ActivateSkill(1);
        }
        //按鍵2：使用清單的第二個技能
        /*if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            ActivateSkill(1);
        }
        ......以此類推*/
    }

    private void ActivateSkill(int index)
    {
        //和物件池比對有無可用同型物件
        SkillBase skill = ObjectPoolManager.Instance.Reuse(skills[index]);

        if (skill != null)
        {//有取到之回收物，呼叫技能裡的重新激活功能(發射者，位置偏移修正)
            skill.ReActive(transform, Vector3.up);
        }
        else
        {//物件池沒有已回收同型物件，新建一個
            Instantiate(skills[index], transform.position + Vector3.up, transform.rotation);
        }
    }
}