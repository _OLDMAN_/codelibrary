using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestOptionalValue : MonoBehaviour
{
    public OptionalValue<Vector3> dir;

    private void Start()
    {
        if (dir.enabled)
        {
            Debug.Log(dir.value);
        }
    }
}
