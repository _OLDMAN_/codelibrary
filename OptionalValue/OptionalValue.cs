using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public struct OptionalValue<T>
{
    [SerializeField] private T _value;
    [SerializeField] private bool _enabled;

    public OptionalValue(T initValue)
    {
        _value = initValue;
        _enabled = true;
    }
    public OptionalValue(T initValue, bool initBool)
    {
        _value = initValue;
        _enabled = initBool;
    }

    public T value => _value;
    public bool enabled => _enabled;
}


/*
使用案例：
需要檢查值的啟用或存在後判斷是否繼續執行後續動作

    public OptionalValue<float> speedLimit;

    if(speedLimit.enabled)
    {
        DoSomething(speedLimit.value);
    }
*/
